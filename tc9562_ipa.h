/* Copyright (c) 2021, The Linux Foundation. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 and
 * only version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#ifndef __TC9562_IPA_H__
#define __TC9562_IPA_H__

#include <linux/ipa.h>
#include <linux/ipa_uc_offload.h>
#include <asm/io.h>
#include <linux/debugfs.h>
#include <linux/in.h>
#include <linux/ip.h>

#include "tc9562mac.h"
#include "dwmac4_dma.h"


#define MIN_VLAN_ID 1
#define MAX_VLAN_ID 4094
#define ULONG unsigned long

/*Neutrino M3 Firmware macros */
/* Debugging count SRAM area start address */
#define M3_DMEM_BASE_PCIE_ADDR			0x40000		/* DMEM PCIE BASE ADDR */
#define M3_DBG_CNT_START            		0x2000F800
#define M3_DMEM_BASE_ADDR			0x20000000	/* DMEM: 0x2000_0000 - 0x2001_0000 */
#define M3_SRAM_IPA_SET_OFST			(M3_DBG_CNT_START - M3_DMEM_BASE_ADDR)
#define M3_SRAM_IPA_SET_DMEM_PCIE_ADDR	(M3_DMEM_BASE_PCIE_ADDR + M3_SRAM_IPA_SET_OFST) + (4*14)

#define M3_DMA_TXCH0_DB_OFST 0x4F800
#define M3_DMA_RXCH0_DB_OFST 0x4F818

#define PCIE_SRAM_BAR_NUM 2
#define IPA_ENABLE_OFFLOAD_MASK 0x1
#define IPA_DISABLE_OFFLOAD_MASK 0xFFFFFFFE
//#define TC9562_M3_DBG_IPA_CAPABLE_MASK 0x1

/* Neutrino register access macros */
#define  TC9562_REG_BASE_ADRS  (priv->dev->base_addr)
#define  TC9562_REG_PHY_BASE_ADRS  (priv->plat->dwc_eth_ntn_reg_pci_base_addr_phy)

/******************** DMA_BUSCFG register access *****************/

#define DMA_BUSCFG_RgWr(data) do {\
        iowrite32(data, (void *)(TC9562_REG_BASE_ADRS + DMA_SYS_BUS_MODE));\
} while(0)

#define DMA_BUSCFG_RgRd(data) do {\
        (data) = ioread32((void *)(TC9562_REG_BASE_ADRS + DMA_SYS_BUS_MODE));\
} while(0)

/*#define DMA_BUSCFG_BLEN16_Mask (unsigned long)(~(~0<<(1)))*/
#define DMA_BUSCFG_BLEN16_Mask (unsigned long)(0x1)
/*#define DMA_BUSCFG_BLEN16_Wr_Mask (unsigned long)(~((~(~0 << (1))) << (3)))*/
#define DMA_BUSCFG_BLEN16_Wr_Mask (unsigned long)(0xfffffff7)

#define DMA_BUSCFG_BLEN16_UdfWr(data) do {\
        unsigned long v;\
        DMA_BUSCFG_RgRd(v);\
        v = ((v & DMA_BUSCFG_BLEN16_Wr_Mask) | ((data & DMA_BUSCFG_BLEN16_Mask)<<3));\
        DMA_BUSCFG_RgWr(v);\
} while(0)

/*#define DMA_BUSCFG_BLEN8_Mask (unsigned long)(~(~0<<(1)))*/
#define DMA_BUSCFG_BLEN8_Mask (unsigned long)(0x1)
/*#define DMA_BUSCFG_BLEN8_Wr_Mask (unsigned long)(~((~(~0 << (1))) << (2)))*/
#define DMA_BUSCFG_BLEN8_Wr_Mask (unsigned long)(0xfffffffb)

#define DMA_BUSCFG_BLEN8_UdfWr(data) do {\
        unsigned long v;\
        DMA_BUSCFG_RgRd(v);\
        v = ((v & DMA_BUSCFG_BLEN8_Wr_Mask) | ((data & DMA_BUSCFG_BLEN8_Mask)<<2));\
        DMA_BUSCFG_RgWr(v);\
} while(0)

/*#define DMA_BUSCFG_BLEN4_Mask (ULONG)(~(~0<<(1)))*/
#define DMA_BUSCFG_BLEN4_Mask (unsigned long)(0x1)
/*#define DMA_BUSCFG_BLEN4_Wr_Mask (ULONG)(~((~(~0 << (1))) << (1)))*/
#define DMA_BUSCFG_BLEN4_Wr_Mask (unsigned long)(0xfffffffd)

#define DMA_BUSCFG_BLEN4_UdfWr(data) do {\
        unsigned long v;\
        DMA_BUSCFG_RgRd(v);\
        v = ((v & DMA_BUSCFG_BLEN4_Wr_Mask) | ((data & DMA_BUSCFG_BLEN4_Mask)<<1));\
        DMA_BUSCFG_RgWr(v);\
} while(0)

/*#define DMA_BUSCFG_WR_OSR_LMT_Mask (ULONG)(~(~0<<(5)))*/
#define DMA_BUSCFG_WR_OSR_LMT_Mask (unsigned long)(0xf)
/*#define DMA_BUSCFG_WR_OSR_LMT_Wr_Mask (ULONG)(~((~(~0 << (5))) << (24)))*/
#define DMA_BUSCFG_WR_OSR_LMT_Wr_Mask (unsigned long)(0xe0ffffff)

#define DMA_BUSCFG_WR_OSR_LMT_UdfWr(data) do {\
        unsigned long v;\
        DMA_BUSCFG_RgRd(v);\
        v = ((v & DMA_BUSCFG_WR_OSR_LMT_Wr_Mask) | ((data & DMA_BUSCFG_WR_OSR_LMT_Mask)<<24));\
        DMA_BUSCFG_RgWr(v);\
} while(0)

#define DMA_BUSCFG_WR_OSR_LMT_UdfRd(data) do {\
        DMA_BUSCFG_RgRd(data);\
        data = ((data >> 24) & DMA_BUSCFG_WR_OSR_LMT_Mask);\
} while(0)

/*#define DMA_BUSCFG_RD_OSR_LMT_Mask (ULONG)(~(~0<<(5)))*/
#define DMA_BUSCFG_RD_OSR_LMT_Mask (unsigned long)(0xf)
/*#define DMA_BUSCFG_RD_OSR_LMT_Wr_Mask (ULONG)(~((~(~0 << (5))) << (16)))*/
#define DMA_BUSCFG_RD_OSR_LMT_Wr_Mask (unsigned long)(0xfff0ffff)

#define DMA_BUSCFG_RD_OSR_LMT_UdfWr(data) do {\
        unsigned long v;\
        DMA_BUSCFG_RgRd(v);\
        v = ((v & DMA_BUSCFG_RD_OSR_LMT_Wr_Mask) | ((data & DMA_BUSCFG_RD_OSR_LMT_Mask)<<16));\
        DMA_BUSCFG_RgWr(v);\
} while(0)

/******************** DMA_TXCHCTL register access *****************/

//Tx - Control
#define DMA_TXCHCTL_RgWr(n, data) { iowrite32(data, (void *)(TC9562_REG_BASE_ADRS + DMA_CHAN_TX_CONTROL(n))); }
#define DMA_TXCHCTL_RgRd(n, data) { (data) = ioread32((void *)(TC9562_REG_BASE_ADRS + DMA_CHAN_TX_CONTROL(n))); }

#define DMA_RXCHINTMASK_MASK    (0x1040)

/*#define DMA_TXCHCTL_OSP_Mask (ULONG)(~(~0<<(1)))*/
#define DMA_TXCHCTL_OSP_Mask (unsigned long)(0x1)
/*#define DMA_TXCHCTL_OSP_Wr_Mask (ULONG)(~((~(~0 << (1))) << (4)))*/
#define DMA_TXCHCTL_OSP_Wr_Mask (unsigned long)(0xffffffef)

#define DMA_TXCHCTL_OSP_UdfWr(n, data) do {\
        unsigned long v;\
        DMA_TXCHCTL_RgRd(n, v);\
        v = ((v & DMA_TXCHCTL_OSP_Wr_Mask) | ((data & DMA_TXCHCTL_OSP_Mask)<<4));\
        DMA_TXCHCTL_RgWr(n, v);\
} while(0)

/*#define DMA_TXCHCTL_TXPBL_Mask (ULONG)(~(~0<<(6)))*/
#define DMA_TXCHCTL_TXPBL_Mask (unsigned long)(0x3f)
/*#define DMA_TXCHCTL_TXPBL_Wr_Mask (ULONG)(~((~(~0 << (6))) << (16)))*/
#define DMA_TXCHCTL_TXPBL_Wr_Mask (unsigned long)(0xffc0ffff)

#define DMA_TXCHCTL_TXPBL_UdfWr(n, data) do {\
        unsigned long v;\
        DMA_TXCHCTL_RgRd(n, v);\
        v = ((v & DMA_TXCHCTL_TXPBL_Wr_Mask) | ((data & DMA_TXCHCTL_TXPBL_Mask)<<16));\
        DMA_TXCHCTL_RgWr(n, v);\
} while(0)
	
/*#define DMA_TXCHCTL_ST_Mask (ULONG)(~(~0<<(1)))*/
#define DMA_TXCHCTL_ST_Mask (unsigned long)(0x1)
/*#define DMA_TXCHCTL_ST_Wr_Mask (ULONG)(~((~(~0 << (1))) << (0)))*/
#define DMA_TXCHCTL_ST_Wr_Mask (unsigned long)(0xfffffffe)

#define DMA_TXCHCTL_ST_UdfWr(n, data) do {\
        unsigned long v;\
        DMA_TXCHCTL_RgRd(n, v);\
        v = ((v & DMA_TXCHCTL_ST_Wr_Mask) | ((data & DMA_TXCHCTL_ST_Mask)<<0));\
        DMA_TXCHCTL_RgWr(n, v);\
} while(0)


/******************** DMA_RXCHCTL register access *****************/

//Rx - Control
#define DMA_RXCHCTL_RgWr(n, data) { iowrite32(data, (void *)(TC9562_REG_BASE_ADRS + DMA_CHAN_RX_CONTROL(n)));   }
#define DMA_RXCHCTL_RgRd(n, data) { (data) = ioread32((void *)(TC9562_REG_BASE_ADRS + DMA_CHAN_RX_CONTROL(n))); }

#define DMA_TXCHINTMASK_MASK    (0x813)

/*#define DMA_RXCHCTL_RXPBL_Mask (ULONG)(~(~0<<(6)))*/
#define DMA_RXCHCTL_RXPBL_Mask (unsigned long)(0x3f)
/*#define DMA_RXCHCTL_RXPBL_Wr_Mask (ULONG)(~((~(~0 << (6))) << (16)))*/
#define DMA_RXCHCTL_RXPBL_Wr_Mask (unsigned long)(0xffc0ffff)

#define DMA_RXCHCTL_RXPBL_UdfWr(n, data) do {\
        unsigned long v;\
        DMA_RXCHCTL_RgRd(n, v);\
        v = ((v & DMA_RXCHCTL_RXPBL_Wr_Mask) | ((data & DMA_RXCHCTL_RXPBL_Mask)<<16));\
        DMA_RXCHCTL_RgWr(n, v);\
} while(0)

/*#define DMA_RXCHCTL_RBSZ_Mask (ULONG)(~(~0<<(14)))*/
#define DMA_RXCHCTL_RBSZ_Mask (unsigned long)(0x3fff)
/*#define DMA_RXCHCTL_RBSZ_Wr_Mask (ULONG)(~((~(~0 << (14))) << (1)))*/
#define DMA_RXCHCTL_RBSZ_Wr_Mask (unsigned long)(0xffff8001)

#define DMA_RXCHCTL_RBSZ_UdfWr(n, data) do {\
        unsigned long v;\
        DMA_RXCHCTL_RgRd(n, v);\
        v = ((v & DMA_RXCHCTL_RBSZ_Wr_Mask) | ((data & DMA_RXCHCTL_RBSZ_Mask)<<1));\
        DMA_RXCHCTL_RgWr(n, v);\
} while(0)

/*#define DMA_RXCHCTL_ST_Mask (ULONG)(~(~0<<(1)))*/
#define DMA_RXCHCTL_ST_Mask (unsigned long)(0x1)
/*#define DMA_RXCHCTL_ST_Wr_Mask (ULONG)(~((~(~0 << (1))) << (0)))*/
#define DMA_RXCHCTL_ST_Wr_Mask (unsigned long)(0xfffffffe)

#define DMA_RXCHCTL_ST_UdfWr(n, data) do {\
        unsigned long v;\
        DMA_RXCHCTL_RgRd(n, v);\
        v = ((v & DMA_RXCHCTL_ST_Wr_Mask) | ((data & DMA_RXCHCTL_ST_Mask)<<0));\
        DMA_RXCHCTL_RgWr(n, v);\
} while(0)

/******************** DMA_CH(n)_Status  register access *****************/

#define DMA_CHSTS_REB_HPOS 21
#define DMA_CHSTS_REB_LPOS 19
#define DMA_CHSTS_TEB_HPOS 18
#define DMA_CHSTS_TEB_LPOS 16
#define DMA_CHSTS_ERI_HPOS 11
#define DMA_CHSTS_ERI_LPOS 11
#define DMA_CHSTS_ETI_HPOS 10
#define DMA_CHSTS_ETI_LPOS 10
#define DMA_CHSTS_RPS_HPOS 8
#define DMA_CHSTS_RPS_LPOS 8
#define DMA_CHSTS_RBU_HPOS 7
#define DMA_CHSTS_RBU_LPOS 7
#define DMA_CHSTS_RI_HPOS 6
#define DMA_CHSTS_RI_LPOS 6
#define DMA_CHSTS_TBU_HPOS 2
#define DMA_CHSTS_TBU_LPOS 2
#define DMA_CHSTS_TPS_HPOS 1
#define DMA_CHSTS_TPS_LPOS 1
#define DMA_CHSTS_TI_HPOS 0
#define DMA_CHSTS_TI_LPOS 0

//Tx - CH status
#define DMA_TXCHSTS_RgWr(n, data) { iowrite32(data, (void *)(TC9562_REG_BASE_ADRS + DMA_CHAN_STATUS(n))); }
#define DMA_TXCHSTS_RgRd(n, data) { (data) = ioread32((void *)(TC9562_REG_BASE_ADRS + DMA_CHAN_STATUS(n))); }

//Rx - CH status
#define DMA_RXCHSTS_RgWr(n, data) { iowrite32(data, (void *)(TC9562_REG_BASE_ADRS + DMA_CHAN_STATUS(n)));   }
#define DMA_RXCHSTS_RgRd(n, data) { (data) = ioread32((void *)(TC9562_REG_BASE_ADRS + DMA_CHAN_STATUS(n))); }

//Hardcoded macros need to change later
#define DMA_CLRCHSTS_RgWr(data) { iowrite32(data, (void *)(TC9562_REG_BASE_ADRS + DMA_CHAN_STATUS(0))); }

/******************** DMA_CH(n)_Interrupt_Enable register access *****************/

//Tx - Interrupt
#define DMA_TXCHINTMASK_RgWr(n, data) { iowrite32(data, (void *)(TC9562_REG_BASE_ADRS + DMA_CHAN_INTR_ENA(n)));   }
#define DMA_TXCHINTMASK_RgRd(n, data) { (data) = ioread32((void *)(TC9562_REG_BASE_ADRS + DMA_CHAN_INTR_ENA(n))); }

//Rx - Interrupt
#define DMA_RXCHINTMASK_RgWr(n, data) { iowrite32(data, (void *)(TC9562_REG_BASE_ADRS + DMA_CHAN_INTR_ENA(n)));   }
#define DMA_RXCHINTMASK_RgRd(n, data) { (data) = ioread32((void *)(TC9562_REG_BASE_ADRS + DMA_CHAN_INTR_ENA(n))); }

#define DMA_CHINTRMSK_RgWr(data) { iowrite32(data, (void *)(TC9562_REG_BASE_ADRS + DMA_CHAN_INTR_ENA(0))); }

//CDEE
#define DMA_TXCHINTMASK_CDEE_Mask (ULONG)(0x1)
#define DMA_TXCHINTMASK_CDEE_Wr_Mask (ULONG)(0xffffdfff)

#define DMA_TXCHINTMASK_CDEE_UdfRd(n, data) do {\
        DMA_TXCHINTMASK_RgRd(n, data);\
        data = ((data >> 13) & DMA_TXCHINTMASK_CDEE_Mask);\
} while(0)

//ERIE
#define DMA_RXCHINTMASK_ERIE_Mask (ULONG)(0x1)
#define DMA_RXCHINTMASK_ERIE_Wr_Mask (ULONG)(0xfffff7ff)

#define DMA_RXCHINTMASK_ERIE_UdfRd(n, data) do {\
        DMA_RXCHINTMASK_RgRd(n, data);\
        data = ((data >> 11) & DMA_RXCHINTMASK_ERIE_Mask);\
} while(0)

//ETIE
#define DMA_TXCHINTMASK_ETCEN_Mask (ULONG)(0x1)
#define DMA_TXCHINTMASK_ETCEN_Wr_Mask (ULONG)(0xfffffbff)

#define DMA_TXCHINTMASK_ETCEN_UdfRd(n, data) do {\
        DMA_TXCHINTMASK_RgRd(n, data);\
        data = ((data >> 10) & DMA_TXCHINTMASK_ETCEN_Mask);\
} while(0)

//RSE
#define DMA_RXCHINTMASK_RSEN_Mask (ULONG)(0x1)
#define DMA_RXCHINTMASK_RSEN_Wr_Mask (ULONG)(0xfffffeff)

#define DMA_RXCHINTMASK_RSEN_UdfRd(n, data) do {\
        DMA_RXCHINTMASK_RgRd(n, data);\
        data = ((data >> 8) & DMA_RXCHINTMASK_RSEN_Mask);\
} while(0)


//RBUE
#define DMA_RXCHINTMASK_UNFEN_Mask (ULONG)(0x1)
#define DMA_RXCHINTMASK_UNFEN_Wr_Mask (ULONG)(0xffffff7f)

#define DMA_RXCHINTMASK_UNFEN_UdfRd(n, data) do {\
        DMA_RXCHINTMASK_RgRd(n, data);\
        data = ((data >> 7) & DMA_RXCHINTMASK_UNFEN_Mask);\
} while(0)

//RIE
#define DMA_RXCHINTMASK_RCEN_Mask (unsigned long)(0x1)
#define DMA_RXCHINTMASK_RCEN_Wr_Mask (unsigned long)(0xffffffbf)

#define DMA_RXCHINTMASK_RCEN_UdfRd(n, data) do {\
        DMA_RXCHINTMASK_RgRd(n, data);\
        data = ((data >> 6) & DMA_RXCHINTMASK_RCEN_Mask);\
} while(0)

//TBUE
#define DMA_TXCHINTMASK_UNFEN_Mask (ULONG)(0x1)
#define DMA_TXCHINTMASK_UNFEN_Wr_Mask (ULONG)(0xfffffffb)

#define DMA_TXCHINTMASK_UNFEN_UdfRd(n, data) do {\
        DMA_TXCHINTMASK_RgRd(n, data);\
        data = ((data >> 2) & DMA_TXCHINTMASK_UNFEN_Mask);\
} while(0)

//TXSE
#define DMA_TXCHINTMASK_TSEN_Mask (ULONG)(0x1)
#define DMA_TXCHINTMASK_TSEN_Wr_Mask (ULONG)(0xfffffffd)

#define DMA_TXCHINTMASK_TSEN_UdfRd(n, data) do {\
        DMA_TXCHINTMASK_RgRd(n, data);\
        data = ((data >> 1) & DMA_TXCHINTMASK_TSEN_Mask);\
} while(0)

//TIE
#define DMA_TXCHINTMASK_TCEN_Mask (ULONG)(0x1)
#define DMA_TXCHINTMASK_TCEN_Wr_Mask (ULONG)(0xfffffffe)

#define DMA_TXCHINTMASK_TCEN_UdfRd(n, data) do {\
        DMA_TXCHINTMASK_RgRd(n, data);\
        data = ((data >> 0) & DMA_TXCHINTMASK_TCEN_Mask);\
} while(0)

/******************** 	DMA_CH(n)_TxDesc_Tail_Pointer *********************/
//Tx - Desc
#define DMA_TXCH_DESC_TAILPTR_RgOffAddr(n) ((volatile ULONG *)(TC9562_REG_BASE_ADRS + DMA_CHAN_TX_END_ADDR(n)))

#define DMA_TXCH_CUR_DESC_RgRd(n, data) { (data) = ioread32((void *)(TC9562_REG_BASE_ADRS + DMA_CHAN_CUR_TX_DESC(n))); }
#define DMA_TXCH_DESC_TAILPTR_RgRd(n, data) { (data) = ioread32((void *)(TC9562_REG_BASE_ADRS + DMA_CHAN_TX_END_ADDR(n))); }

/******************** 	DMA_CH(n)_RxDesc_Tail_Pointer *********************/

//Rx - Desc
#define DMA_RXCH_DESC_TAILPTR_RgOffAddr(n) ((volatile ULONG *)(TC9562_REG_BASE_ADRS + DMA_CHAN_RX_END_ADDR(n)))

#define DMA_RXCH_CUR_DESC_RgWr(n, data) { iowrite32(data, (void *)(TC9562_REG_BASE_ADRS + DMA_CHAN_CUR_RX_DESC(n)));   }
#define DMA_RXCH_CUR_DESC_RgRd(n, data) { (data) = ioread32((void *)(TC9562_REG_BASE_ADRS + DMA_CHAN_CUR_RX_DESC(n))); }
#define DMA_RXCH_DESC_TAILPTR_RgRd(n, data) { (data) = ioread32((void *)(TC9562_REG_BASE_ADRS + DMA_CHAN_RX_END_ADDR(n))); }

/*********************************************************************/
/* IPA related macros */

#define IPA_TX_DESC_CNT 128 /*Increase the TX desc count to 128 for IPA offload*/
#define IPA_RX_DESC_CNT 128 /*Increase the RX desc count to 128 for IPA offload*/
#define TC9562_RX_DMA_CH_0 (0)
#define TC9562_TX_DMA_CH_0 (0)

/* Add IPA specific Macros to access the DMA and virtual address to be provided to IPA uC*/
#define GET_TX_BUFF_DMA_ADDR(chInx, dInx) (priv->tx_queue[(chInx)].ipa_tx_buff_dma_addrs[(dInx)])
#define GET_TX_BUFF_LOGICAL_ADDR(chInx, dInx) (priv->tx_queue[(chInx)].ipa_tx_sk_buff_addrs[(dInx)])
#define GET_TX_BUFF_DMA_POOL_BASE_ADRR(chInx) (priv->tx_queue[(chInx)].ipa_tx_buff_dma_addrs)

/* Add IPA specific Macros to access the DMA address to be provided to IPA uC*/
#define GET_RX_BUFF_DMA_ADDR(chInx, dInx) (priv->rx_queue[(chInx)].ipa_rx_buff_dma_addrs[(dInx)])
#define GET_RX_BUFF_POOL_BASE_ADRR(chInx) (priv->rx_queue[(chInx)].ipa_rx_buff_dma_addrs)

#define GET_RX_DESC_IDX(chInx, desc) (((desc) - priv->rx_queue[TC9562_RX_DMA_CH_0].dma_rx_phy)/(sizeof(struct dma_desc)))
#define GET_TX_DESC_IDX(chInx, desc) (((desc) - priv->tx_queue[TC9562_RX_DMA_CH_0].dma_tx_phy)/(sizeof(struct dma_desc)))


/* use TAMAP 1 region for M3 to access memory mapped IPA doorbell registers*/
#define TC9562_HOST_TAMAP_MEM_LENGTH	(1<<12)
#define TC9562_PCIE_REGION_MEM_MAP_BASE	0x60000000
#define NTN_GET_TAMAP_MASK_BITS(length, no_of_bits)	do {\
	int len = length;\
	no_of_bits = 64;\
	while(!(len&0x1)){\
		len>>=1;\
		no_of_bits--;\
	};\
} while(0)
#define IPA_UC_REG_BASE	0x7962000 /* IPA uC base register address*/
#define TC9562_ETH_FRAME_LEN_IPA	(1<<11) /*IPA can support 2KB max pkt length*/

enum{
 TC9562_IPA_VLAN_DISABLE_CMD= 0,
 TC9562_IPA_VLAN_ENABLE_CMD=1,
};
struct ifr_data_struct_ipa {
	unsigned int chInx_tx_ipa;
	unsigned int chInx_rx_ipa;
	unsigned int cmd;
	unsigned short vlan_id;
};

#ifdef TC9562_IPA_OFFLOAD

#define TC9562_HOST_IPA_CAPABLE 1
/* IPA Ready client callback. Called by IPA when its ready */
void tc9562_ipa_uc_ready_cb(void *user_data);

int tc9562_enable_ipa_offload(struct tc9562mac_priv *pdata);
int tc9562_disable_ipa_offload(struct tc9562mac_priv *pdata);
int tc9562_disable_enable_ipa_offload(struct tc9562mac_priv *pdata,int chInx_tx_ipa,int chInx_rx_ipa);

/* Initialize Offload data path and add partial headers */
int tc9562_ipa_offload_init(struct tc9562mac_priv *pdata);

/* Cleanup Offload data path */
int tc9562_ipa_offload_cleanup(struct tc9562mac_priv *pdata);

/* Connect Offload Data path */
int tc9562_ipa_offload_connect(struct tc9562mac_priv *pdata);

/* Disconnect Offload Data path */
int tc9562_ipa_offload_disconnect(struct tc9562mac_priv *pdata);

/* Create Debugfs Node */
int tc9562_ipa_create_debugfs(struct tc9562mac_priv *pdata);

/* Cleanup Debugfs Node */
int tc9562_ipa_cleanup_debugfs(struct tc9562mac_priv *pdata);

void tc9562_ipa_stats_read(struct tc9562mac_priv *pdata);
int tc9562_ipa_offload_suspend(struct tc9562mac_priv *pdata);
int tc9562_ipa_offload_resume(struct tc9562mac_priv *pdata);

int tc9562_ipa_ready(struct tc9562mac_priv *pdata);

#else /* TC9562_IPA_OFFLOAD */

#define TC9562_HOST_IPA_CAPABLE 0

static inline int tc9562_ipa_offload_init(struct tc9562mac_priv *pdata)
{
	return -EPERM;
}

static inline void tc9562_ipa_uc_ready_cb(void *user_data)
{
	return;
}

static inline int tc9562_enable_ipa_offload(struct tc9562mac_priv *pdata)
{
	return -EPERM;
}

static inline int tc9562_disable_ipa_offload(struct tc9562mac_priv *pdata)
{
	return -EPERM;
}

static inline int tc9562_disable_enable_ipa_offload(struct tc9562mac_priv *pdata,int chInx_tx_ipa,int chInx_rx_ipa)
{
	return -EPERM;
}

static inline void tc9562_ipa_stats_read(struct tc9562mac_priv *pdata)
{
	return;
}

static inline int tc9562_ipa_offload_suspend(struct tc9562mac_priv *pdata)
{
	return -EPERM;
}

static inline int tc9562_ipa_offload_resume(struct tc9562mac_priv *pdata)
{
	return -EPERM;
}

static inline int tc9562_ipa_ready(struct tc9562mac_priv *pdata)
{
	return -EPERM;
}

#endif /* TC9562_IPA_OFFLOAD */

#endif
