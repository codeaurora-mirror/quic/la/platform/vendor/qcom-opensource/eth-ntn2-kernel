/* Copyright (c) 2021, The Linux Foundation. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 and
 * only version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

/*!@file: tc9562mac_plt_init.c
 * @brief: Driver functions.
 */


#include <linux/kernel.h>
#include <linux/platform_device.h>
#include <linux/module.h>
#include <linux/gpio.h>
#include <linux/regulator/consumer.h>
#include <linux/of_gpio.h>
#include <linux/pm.h>
#include <linux/pm_wakeup.h>
#include <linux/sched.h>
#include <linux/pm_qos.h>
#include <linux/pm_runtime.h>
#include <linux/esoc_client.h>
#include <linux/pinctrl/consumer.h>
#ifdef CONFIG_PCI_MSM
#include <linux/msm_pcie.h>
#else
#include <mach/msm_pcie.h>
#endif
#include "tc9562mac.h"

#define NTN_PWR_GPIO_NAME   "ntn-supply-enable-gpio"

struct list_head ntn2_plt_data_list;

static int tc9562mac_plt_probe(struct platform_device *pdev);
static int tc9562mac_plt_remove(struct platform_device *pdev);


static int setup_gpio_common(struct device *dev, const char *name, int *gpio,
                                                                int direction, int value)
{
    int ret = 0;

    if (of_find_property(dev->of_node, name, NULL)) {
        *gpio = ret = of_get_named_gpio(dev->of_node, name, 0);
        if (ret >= 0) {
            ret = gpio_request(*gpio, name);
            if (ret) {
                NMSGPR_ALERT("%s: Can't get GPIO %s, ret = %d\n", __func__, name, *gpio);
                *gpio = -1;
                return -1;
            }

            ret = gpio_direction_output(*gpio, direction);
            if (ret) {
                NMSGPR_ALERT("%s: Can't set GPIO %s direction, ret = %d\n", __func__, name, ret);
                return -1;
            }
            gpio_set_value(*gpio, value);
        } else {
            if (ret == -EPROBE_DEFER)
                NMSGPR_ALERT("get NTN_PWR_GPIO_NAME probe defer\n");
            else
                NMSGPR_ALERT("can't get gpio %s ret %d", name, ret);
            return -1;
        }
    } else {
        NMSGPR_ALERT("can't find gpio %s", name);
    }
    return 0;
}

static int tc9562mac_init_gpios(struct device *dev, struct tc9562_plt_data *plt_data)
{
    int ret = 0;

    if (!dev)
        return -EINVAL;

    plt_data->supply_gpio_num = -1;

    /* Configure GPIOs required for GPIO regulator control */
    ret = setup_gpio_common(dev, NTN_PWR_GPIO_NAME, &plt_data->supply_gpio_num, 0x1, 0x1);
    if (ret)
        goto exit_error;

    return ret;

exit_error:
    if (gpio_is_valid(plt_data->supply_gpio_num))
        gpio_free(plt_data->supply_gpio_num);
    plt_data->supply_gpio_num = -1;
    return ret;
}

static void tc9562mac_free_regs_and_gpios(struct tc9562_plt_data *plt_data)
{
    if (gpio_is_valid(plt_data->supply_gpio_num)) {
        /* Power off Neutrino when it is not in use */
        gpio_set_value(plt_data->supply_gpio_num, 0x0);
        gpio_free(plt_data->supply_gpio_num);
    }
}

static int tc9562mac_plt_probe(struct platform_device *pdev)
{
    int ret = 0;
    struct device *dev = &pdev->dev;
    struct tc9562_plt_data *plt_data;

    DBGPR_FUNC( "Entry:%s.\n", __func__);

    plt_data = devm_kzalloc(&pdev->dev, sizeof(*plt_data), GFP_KERNEL);
    if (!plt_data)
        return -ENOMEM;

    plt_data->pldev = pdev;

    ret = tc9562mac_init_gpios(dev, plt_data);
    if (ret) {
        NMSGPR_ALERT("%s: Failed to init GPIOs\n", __func__);
        goto tc9562mac_err_pcie_probe;
    }

    /* read bus number */
        ret = of_property_read_u32(dev->of_node, "qcom,ntn-bus-num", &plt_data->bus_num);
        if (ret) {
                plt_data->bus_num = -1;
                NMSGPR_ALERT( "%s: Failed to find PCIe bus number from device tree!\n", __func__);
        } else {
                NDBGPR_L1( "%s: Found PCIe bus number! %d\n", __func__, plt_data->bus_num);
        }

    /* issue PCIe enumeration */
    ret = of_property_read_u32(dev->of_node, "qcom,ntn-rc-num", &plt_data->rc_num);
    if (ret) {
        NMSGPR_ALERT( "%s: Failed to find PCIe RC number!, RC=%d\n", __func__, plt_data->rc_num);
        goto tc9562mac_err_pcie_probe;
    } else {
        NDBGPR_L1( "%s: Found PCIe RC number! %d\n", __func__, plt_data->rc_num);
        ret = msm_pcie_enumerate(plt_data->rc_num);
        if (ret) {
            NMSGPR_ALERT( "%s: Failed to enable PCIe RC%x!\n", __func__, plt_data->rc_num);
            goto tc9562mac_err_pcie_probe;
        } else {
            NMSGPR_ALERT( "%s: PCIe enumeration for RC number %d successful!\n", __func__, plt_data->rc_num);
        }
    }

    list_add_tail(&plt_data->node, &ntn2_plt_data_list);

    NMSGPR_INFO("%s probe success", __func__);
    DBGPR_FUNC( "Exit:%s.\n", __func__);
    return ret;

tc9562mac_err_pcie_probe:
    tc9562mac_free_regs_and_gpios(plt_data);
    NMSGPR_ALERT("%s probe failed", __func__);
    return ret;
}

static int tc9562mac_plt_remove(struct platform_device *pdev)
{
    int ret = 0;
    struct tc9562_plt_data *plt_data;
    DBGPR_FUNC( "Entry:%s.\n", __func__);

    list_for_each_entry(plt_data, &ntn2_plt_data_list, node) {
        if (plt_data->pldev == pdev) {
            tc9562mac_free_regs_and_gpios(plt_data);
            break;
        }
    }

    DBGPR_FUNC( "Exit:%s.\n", __func__);
    return ret;
}

static struct of_device_id tc9562mac_eth_match[] = {
    {   .compatible = "qcom,ntn2_avb",
    },
    {}
};

static struct platform_driver tc9562mac_eth_driver = {
    .probe  =   tc9562mac_plt_probe,
    .remove =   tc9562mac_plt_remove,
    .driver =   {
        .name   =   "ntn2_avb",
        .owner  =   THIS_MODULE,
        .of_match_table =   tc9562mac_eth_match,
	},
};

static int __init tc9562mac_plt_init(void)
{
    int ret = 0;
    INIT_LIST_HEAD(&ntn2_plt_data_list);
    DBGPR_FUNC("Entry:%s.\n", __func__);
    ret = platform_driver_register(&tc9562mac_eth_driver);

    if (ret == 0) {
        ret = tc9562_init_module();
    }
    DBGPR_FUNC( "Exit:%s.\n", __func__);
    return ret;
}

static void __exit tc9562mac_plt_exit(void)
{
    DBGPR_FUNC( "Entry:%s.\n", __func__);
    tc9562_exit_module();
    platform_driver_unregister(&tc9562mac_eth_driver);
    DBGPR_FUNC( "Exit:%s.\n", __func__);
}

module_init(tc9562mac_plt_init);
module_exit(tc9562mac_plt_exit);

MODULE_AUTHOR("TC9562 PCI Ethernet driver");
MODULE_DESCRIPTION("TAEC/TDSC");
MODULE_LICENSE("GPL v2");

